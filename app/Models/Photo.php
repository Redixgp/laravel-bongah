<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Photo extends Model
{
    use HasFactory;

    protected $table = 'banner_photos';

    protected $fillable = ['name' , 'path' , 'thumbnail_path'];

//    protected $baseDir = 'banner/photos';

    protected $file;

    public function banner()
    {
        return $this->belongsToMany(Banner::class);
    }

    public static function fromFile(UploadedFile $file)
    {
        $photo = new static;

        $photo->file = $file;

        $photo->fill([
            'name'              => $photo->fileName(),
            'path'              => $photo->filePath(),
            'thumbnail_path'    => $photo->thumbnailPath()
        ]);

        return $photo;
    }

    public function fileName()
    {
        $name = sha1(time() . $this->file->getClientOriginalName());
        $extension = $this->file->getClientOriginalExtension();
        return "{$name}.{$extension}";
    }

    public function filePath()
    {
        return $this->baseDir() . '/' . $this->fileName();
    }

    public function thumbnailPath()
    {
        return $this->baseDir() . '/tn-' . $this->fileName();
    }

    public function baseDir()
    {
        return 'banner/photos';
    }

    public function upload()
    {
        $this->file->move($this->baseDir(), $this->fileName());

        $this->makeThumbnail();

        return $this;
    }


    public function makeThumbnail()
    {
        \Intervention\Image\Facades\Image::make($this->filePath())->fit(200)->save($this->thumbnailPath());

        return $this;
    }
}
