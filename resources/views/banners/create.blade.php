@extends('layout')

@section('content')

    <h1>Selling Your Home?!</h1>

    <hr>

    <div class="row">
        <div>
            <form action="{{ route('banners.store') }}" method="POST" role="form">

                @include('banners.form')

                @include('partials.errors')

            </form>
        </div>
    </div>


@stop
