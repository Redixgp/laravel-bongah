
@if(session()->has('flash_message'))
    <script>
        swal.fire({
            icon: '{{ session('flash_message.level') }}',
            title: '{{ session('flash_message.title') }}',
            text: '{{ session('flash_message.message') }}',
            showConfirmButton: false,
            timer: 2000
        })
    </script>
@endif

@if(session()->has('flash_message_overlay'))
    <script>
        swal.fire({
            icon: '{{ session('flash_message_overlay.level') }}',
            title: '{{ session('flash_message_overlay.title') }}',
            text: '{{ session('flash_message_overlay.message') }}',
            confirmButtonText: 'OK!'
        })
    </script>
@endif
